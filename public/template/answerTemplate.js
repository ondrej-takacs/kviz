import Answer from "../js/answer.js";
import Question from "../js/question.js";

export default class AnswerTemplate {
  /**
   * @param {Question} question
   * @param {Answer} answer
   */
  static render(question, answer) {
    return `
      <input type="${question.type}" class="answer-input"
        name="${question.index}" id="${question.index}-${answer.index}"
        ${answer.isChecked
          ? 'checked="checked'
          : ''}
        ${question.isDisabled
          ? 'disabled'
          : ''}
      >
      <label for="${question.index}-${answer.index}">
        ${answer.type === Answer.TYPE_IMAGE
          ? `<img src="${answer.imgPath}">`
          : `${answer.text}`}
      </label>`;
  }
}
