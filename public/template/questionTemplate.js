import Question from '../js/question.js';

export default class QuestionTemplate {
  /**
   * @param {Question} question
   */
  static render(question) {
    return `
  <div class="question ${question.isTypeImage() ? 'question-image' : ''}">
    <span class="question-order">${question.index + 1}.</span>
    <span class="text">
      ${question.text}
    </span>
    <ul class="answers">
      ${question.Answers.map((answer) => `
        <li class="answer">
          ${answer.render(question)}
        </li>`
      ).join('')}
    </ul>
  </div>
`;
  }
}
