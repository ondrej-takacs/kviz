import Quiz from '../js/quiz.js';

export default class QuizTemplate {
  /**
   * @param {Quiz} quiz
   */
  static render(quiz) {
    return `
  <div class="quiz">
    <h2>
        ${quiz.header}
    </h2>
    
    ${quiz.renderCurrentQuestion()}
    ${quiz.hasPreviousQuestion()
      ? `<button class="quiz-button" id="quiz-previous">❮ Předchozí </button>`
      : ''}
    ${quiz.hasNextQuestion()
      ? `<button class="quiz-button" id="quiz-next">Další ❯</button>`
      : `<button class="quiz-button" id="quiz-send">Odeslat ❯❯</button>`}  
    
 </div>
`;
  }
}
