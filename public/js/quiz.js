import Question from './question.js';
import QuizTemplate from '../template/quizTemplate.js';
import Result from './result.js';

export default class Quiz {
    /**
     * @type {string}
     */
    header;

    /**
     * @type {Array<Question>}
     */
    Questions = [];

    /**
     * @type {Question}
     */
    CurrentQuestion;

    /**
     * @type {number}
     */
    questionNumber = 0;

    /**
     * @param {string} header
     */
    constructor(header) {
      this.header = header;
    }

    /**
     * @param {Array<Question>} Questions
     * @returns {Quiz}
     */
    setQuestions(Questions) {
      if (!$.isArray(Questions)) {
        return this;
      }
      this.Questions = Questions;
      if (this.Questions.length > 0) {
        this.CurrentQuestion = this.Questions[0];
        this.questionNumber = 0;
      }
      return this;
    }

    /**
     * 
     * @param {string} type
     * @param {string} text
     * @param {number} points
     */
    addQuestion(type, text, points) {
      const question = new Question();
      question.type = type;
      question.text = text;
      question.index = this.Questions.length;
      question.points = points;
      this.Questions.push(question);
      this.CurrentQuestion = this.Questions[0];
      return question;
    }

    /**
     * @returns {Question}
     */
    nextQuestion() {
      if (this.hasNextQuestion()) {
        this.questionNumber += 1;
        this.CurrentQuestion = this.Questions[this.questionNumber];
      }
      return this.CurrentQuestion;
    }

    /**
     * @returns {Question}
     */
    previousQuestion() {
      if (this.hasPreviousQuestion()) {
        this.questionNumber -= 1;
        this.CurrentQuestion = this.Questions[this.questionNumber];
      }
      return this.CurrentQuestion;
    }

    /**
     * @returns {string}
     */
    render() {
      return QuizTemplate.render(this);
    }

    afterRender() {
      const quiz = this;
      const result = new Result(quiz);
      $('#quiz-previous').click(() => {
        result.saveResults();
        quiz.previousQuestion();
        $('#quiz-content').html(quiz.render());
        quiz.afterRender();
      });
      $('#quiz-next').click(() => {
        result.saveResults();
        quiz.CurrentQuestion.disableIfAnswered();
        quiz.nextQuestion();
        $('#quiz-content').html(quiz.render());
        quiz.afterRender();
      });
      $('#quiz-send').click(() => {
        result.saveResults();
        quiz.CurrentQuestion.disableIfAnswered();
        $('#quiz-content').html(result.render());
        result.afterRender();
      });
    }

    hasNextQuestion() {
      return this.questionNumber + 1 < this.Questions.length;
    }

    hasPreviousQuestion() {
      return this.questionNumber > 0;
    }

    /**
     * @returns {string}
     */
    renderCurrentQuestion() {
      const content = this.CurrentQuestion.render();
      return content;
    }

    /**
     * @returns {string}
     */
    renderNextQuestion() {
      this.nextQuestion();
      return this.CurrentQuestion.render();
    }

    /**
     * @returns {string}
     */
    renderPreviousQuestion() {
      this.previousQuestion();
      return this.CurrentQuestion.render();
    }

    /**
     * @param {number} questionIndex
     * @param {number} answerIndex
     * @param {boolean} isChecked
     */
    saveAnswer(questionIndex, answerIndex, isChecked) {
      const question = this.Questions[questionIndex];
      question.saveAnswer(answerIndex, isChecked);
    }
}
