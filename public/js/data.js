import Quiz from './quiz.js';
import Question from './question.js';
import Answer from './answer.js';

export default class Data {
  /**
     * @returns Quiz
     */
  static getOtevrenaZahradaQuiz() {
    const quiz = new Quiz('Kvíz o Otevřené zahradě');

    // #region 1. question
    quiz.addQuestion(
      Question.TYPE_RADIO,
      'Kdy byla Otevřená zahrada poprvé zpřístupněna veřejnosti?',
      1,
    )
      .addAnswerText(true, 'Jaro 2013')
      .addAnswerText(false, 'Podzim 2006')
      .addAnswerText(false, 'Léto 2014')
      .addAnswerText(false, 'Jaro 2001');
    // #endregion

    // #region 2. question
    quiz.addQuestion(
      Question.TYPE_CHECKBOX,
      'Která zvířata se chovají v Otevřené zahradě?',
      4,
    )
      .addAnswerText(true, 'Slepice')
      .addAnswerText(false, 'Morčata')
      .addAnswerText(true, 'Včely')
      .addAnswerText(true, 'Ovce')
      .addAnswerText(true, 'Králici')
      .addAnswerText(false, 'Krávy')
      .addAnswerText(false, 'Holubi');
    // #endregion

    // #region 3. question
    quiz.addQuestion(
      Question.TYPE_CHECKBOX,
      'Které z následujících fotek jsou z Otevřené zahrady?',
      2,
    )
      .addAnswerImgPath(true, 'img/garden1.jpg')
      .addAnswerImgPath(false, 'img/garden3.jpg')
      .addAnswerImgPath(false, 'img/garden4.jpg')
      .addAnswerImgPath(false, 'img/garden5.jpg')
      .addAnswerImgPath(false, 'img/garden6.jpg')
      .addAnswerImgPath(false, 'img/garden7.jpg')
      .addAnswerImgPath(true, 'img/garden2.jpg')
      .addAnswerImgPath(false, 'img/garden8.jpg')
      .addAnswerImgPath(false, 'img/garden9.jpg')
      .addAnswerImgPath(false, 'img/garden10.jpg');
    // #endregion

    // #region 4. question
    quiz.addQuestion(
      Question.TYPE_RADIO,
      'V jakém městě se nachází Otevřená zahrada?',
      1,
    )
      .addAnswerText(false, 'Praha')
      .addAnswerText(true, 'Brno')
      .addAnswerText(false, 'Ostrava')
      .addAnswerText(false, 'Olomouc');
    // #endregion

    // #region 5. question
    quiz.addQuestion(
      Question.TYPE_CHECKBOX,
      'Součástí areálu je:',
      3,
    )
      .addAnswerText(true, 'Unikátní pasivní budova se zelenou střechou')
      .addAnswerText(true, 'Bylinková zahrada')
      .addAnswerText(false, 'Dětské koupaliště')
      .addAnswerText(true, 'Výuková zahrada čtyř živlů');
    // #endregion

    return quiz;
  }
}
