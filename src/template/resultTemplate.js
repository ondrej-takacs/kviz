import Result from '../js/result.js';

export default class ResultTemplate {
  /**
   * @param {Result} result
   */
  static render(result) {
    return html`
    <h2>
        Přehled získaných bodů
    </h2>

    ${result.Quiz.Questions.map((question) =>
      html`
      <div class="question result">
        <span class="question-order">${question.index + 1}.</span>
        <span class="text">
          ${question.text}
        </span>
        <span class="question-result">
          <span class="question-result-points">${Result.getQuestionResultPoints(question)}</span>
          /
          <span class="question-points">${question.points}</span>
        </span>
      </div>`).join('')}
    <div class="result-total-points">
      Bodů celkem: ${result.getQuizPoints()} / ${result.getQuizMaxPoints()}
    </div>
    <div class="rerun">
      <button id="rerun-quiz">Znovu spustit kvíz</button>
    </div>
    `;
  }
}
