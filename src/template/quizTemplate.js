import Quiz from '../js/quiz.js';

export default class QuizTemplate {
  /**
   * @param {Quiz} quiz
   */
  static render(quiz) {
    return html`
  <div class="quiz">
    <h2>
        ${quiz.header}
    </h2>
    
    ${quiz.renderCurrentQuestion()}
    ${quiz.hasPreviousQuestion()
      ? html`<button class="quiz-button" id="quiz-previous">❮ Předchozí </button>`
      : ''}
    ${quiz.hasNextQuestion()
      ? html`<button class="quiz-button" id="quiz-next">Další ❯</button>`
      : html`<button class="quiz-button" id="quiz-send">Odeslat ❯❯</button>`}  
    
 </div>
`;
  }
}
