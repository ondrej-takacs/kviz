import ResultTemplate from '../template/resultTemplate.js';
import Quiz from './quiz.js';
import Question from './question.js';
import Data from './data.js';

export default class Result {
  /**
   * @type {Quiz}
   */
  Quiz;

  constructor(quiz) {
    this.Quiz = quiz;
  }

  /**
   * @returns {string}
   */
  render() {
    return ResultTemplate.render(this);
  }

  afterRender() {
    $('#rerun-quiz').click(() => {
      this.Quiz = Data.getOtevrenaZahradaQuiz();
      $('#quiz-content').html(this.Quiz.render());
      this.Quiz.afterRender();
    });
  }

  saveResults() {
    const result = this;
    $('.answer-input').map((index, answer) => {
      result.saveAnswer(answer.id, answer.checked);
    });
  }

  /**
   * @param {string} id
   * @param {boolean} isChecked
   */
  saveAnswer(id, isChecked) {
    const idArr = id.split('-');
    const questionId = idArr[0];
    const answerId = idArr[1];
    this.Quiz.saveAnswer(questionId, answerId, isChecked);
  }

  /**
   * @param {Question} question
   */
  static getQuestionResult(question) {
    const countCorrectCheckedAnswers = question.Answers.filter((answer) =>
      answer.isChecked && answer.isCorrect).length;
    const countIncorrectCheckedAnswers = question.Answers.filter((answer) =>
      answer.isChecked && !answer.isCorrect).length;
    const countCorectAnswers = question.Answers.filter((answer) =>
      answer.isCorrect).length;
    let result = ((countCorrectCheckedAnswers - countIncorrectCheckedAnswers)
      / countCorectAnswers) * 100;
    if (result < -100) {
      result = -100;
    }
    return result;
  }

  /**
   * @param {Question} question
   */
  static getQuestionResultPoints(question) {
    return Math.floor((question.points * Result.getQuestionResult(question)) / 100);
  }

  getQuizPoints() {
    let totalPoints = 0;
    this.Quiz.Questions.forEach((question) =>
      totalPoints += Result.getQuestionResultPoints(question));
    return totalPoints;
  }

  getQuizMaxPoints() {
    let totalMaxPoints = 0;
    this.Quiz.Questions.forEach((question) =>
      totalMaxPoints += question.points);
    return totalMaxPoints;
  }
}
