import Answer from './answer.js';
import QuestionTemplate from '../template/questionTemplate.js';

export default class Question {
  /**
   * @type {string}
   */
  static TYPE_RADIO = 'radio';

  /**
   * @type {string}
   */
  static TYPE_CHECKBOX = 'checkbox';

  /**
   * @type {string}
   */
  type;

  /**
   * @type {string}
   */
  text;

  /**
   * @type {number}
   */
  index;

  /**
   * @type {boolean}
   */
  isDisabled;

  /**
   * @type {Array<Answer>}
   */
  Answers = [];

  /**
   * @type {number}
   */
  points;

  /**
     * @returns {string}
     */
  render() {
    return QuestionTemplate.render(this);
  }

  /**
   * @param {boolean} isCorrect
   * @param {string} text
   * @returns {Question}
   */
  addAnswerText(isCorrect, text) {
    const answer = new Answer();
    answer.index = this.Answers.length;
    answer.setText(text);
    answer.isCorrect = isCorrect;
    this.Answers.push(answer);
    return this;
  }

  /**
   * @param {boolean} isCorrect
   * @param {string} imgPath
   * @returns {Question}
   */
  addAnswerImgPath(isCorrect, imgPath) {
    const answer = new Answer();
    answer.index = this.Answers.length;
    answer.setImgPath(imgPath);
    answer.isCorrect = isCorrect;
    this.Answers.push(answer);
    return this;
  }

  isTypeImage() {
    return this.Answers.some((answer) => answer.type === Answer.TYPE_IMAGE);
  }

  /**
   * @param {number} answerIndex
   * @param {boolean} isChecked
   */
  saveAnswer(answerIndex, isChecked) {
    const answer = this.Answers[answerIndex];
    answer.isChecked = isChecked;
  }

  disableIfAnswered() {
    if (this.Answers.some((answer) => answer.isChecked)) {
      this.isDisabled = true;
    }
  }
}
