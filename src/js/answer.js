import AnswerTemplate from '../template/answerTemplate.js';
import Question from './question.js';

export default class Answer {
    /**
    * @type {string}
    */
    static TYPE_TEXT = 'text';

    /**
    * @type {string}
    */
    static TYPE_IMAGE = 'image';

    /**
     * @type {number}
     */
    index;

    /**
     * @type {boolean}
     */
    isCorrect;

    /**
     * @type {boolean}
     */
    isChecked = false;

    /**
     * @type {string}
     */
    type;

    /**
     * @type {string}
     */
    text;

    /**
     * @type {string}
     */
    imgPath;

    /**
     * @param {string} text
     * @returns {Answer}
     */
    setText(text) {
      this.text = text;
      this.type = Answer.TYPE_TEXT;
      return this;
    }

    /**
     * @param {string} imgPath
     * @returns {Answer}
     */
    setImgPath(imgPath) {
      this.imgPath = imgPath;
      this.type = Answer.TYPE_IMAGE;
      return this;
    }

    /**
     * @param {Question} question
     * @returns {string}
     */
    render(question) {
      return AnswerTemplate.render(question, this);
    }
}
