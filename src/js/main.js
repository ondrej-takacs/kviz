import Data from './data.js';
import Quiz from './quiz.js';

let main;

class Main {
  /**
   * @type {Quiz}
   */
  Quiz;

  constructor() {
    const quizString = localStorage.getItem('quiz');
    // TODO vypnout
    if (quizString == null || true) {
      this.Quiz = Data.getOtevrenaZahradaQuiz();
      localStorage.setItem('quiz', JSON.stringify(this.Quiz));
    } else {
      this.Quiz = JSON.parse(quizString);
    }
  }

  static init() {
    $('#quiz-content').html(main.Quiz.render());
    main.Quiz.afterRender();
  }
}

main = new Main();
$(document).ready(Main.init);
