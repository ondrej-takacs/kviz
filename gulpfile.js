function defaultTask(cb) {
  let gulp = require('gulp');
  let replace = require('gulp-replace');

  gulp.src('src/js/**/*.js')
    .pipe(replace('html`', '`')) // Replace 'html`' with just '`'
    .pipe(gulp.dest('public/js'));

  gulp.src('src/template/**/*.js')
    .pipe(replace('html`', '`')) // Replace 'html`' with just '`'
    .pipe(gulp.dest('public/template'));

  cb();
}

exports.default = defaultTask;
