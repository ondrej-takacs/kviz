# Kvíz

Kvíz napsaný pomocí HTML, CSS a JavaScript.

Zadání:
Za pomocí HTML, CSS a skriptu (JavaScript, jQuery, nebo Angular JS) vytvořte kvíz s 5 otázkami. Každá otázka má různý počet různě bodovaných odpovědí. Obsah kvízu můžete připravit dle svých invencí. Musíte splnit toto zadání:
1. Aspoň jedna otázka má volbu jen jedné možnosti (radiobutton), kdy správná odpověď dává plusový bod, špatná mínusový bod. 
2. Aspoň jedna otázka má možnost více odpovědí (checkbox), kdy každé správné zaškrtnutí dává plusový bod, špatné zaškrtnutí mínusový bod. 
3. Aspoň v jedné otázce odpovídá dotazovaný označením 2 z několika obrázků - kromě položené otázky, obrázků a možnosti přechodů mezi otázkami nejsou viditelné jiné prvky.
4. Dotazovaný prochází mezi otázkami tak, že vidí vždy jen jednu z nich.
5. Po zodpovězení otázky a přechodu na další nesmí mít možnost zadanou odpověď změnit. 
6. Za poslední otázkou následuje přehled získaných bodů za jednotlivé otázky kvízu a celkový počet bodů.

Doplnění:
Každá otázka má různý počet různě bodovaných odpovědí = každá otázka kvízu je bodovaná jiným počtem bodů a zároveň některé otázky mají více správných odpovědí 
... kdy správná odpověď dává plusový bod, špatná mínusový bod = platí pro otázky, které mají více možností odpovědí, počet plusových či minusových bodů je lhostejný (může být jeden i více)
otázka s obrázky se buduje na stejném principu jako otázka s checkboxy

Analýza:
- otázka má zadaný počet bodů, přepočítavají se podle procenta správnosti odpovědi